# vue-travel-web

![JS](https://camo.githubusercontent.com/54682509890a3bd8b645982e1a6cd7e8c3d94e98/68747470733a2f2f696d672e736869656c64732e696f2f62616467652f4a6176615363726970742d65786572636973652d4630444234462e737667)

- [API](https://data.kcg.gov.tw/api/action/datastore_search?resource_id=92290ee5-6e61-456f-80c0-249eae2fcc97)

## Plugins

- (Vue-dropdwon)[https://www.npmjs.com/package/vue-dropdowns]

## Update

| Time       | Note             |
| ---------- | ---------------- |
| 2020-04-23 | Create and setup |

## Project setup

```
npm install
```

### Compiles and hot-reloads for development

```
npm run serve
```

### Compiles and minifies for production

```
npm run build
```

### Customize configuration

See [Configuration Reference](https://cli.vuejs.org/config/).
